// @codekit-prepend "libs/jquery.js", "libs/jquery-ui.min.js"

// Codigo js para página Intra VR7

function detalles() {
	var dets = {};

	dets.errores_btn = document.querySelector(".js--detalle__errores_btn");
	dets.estats_btn = document.querySelector(".js--detalle__estats_btn");
	dets.info = document.querySelector(".detalle--informacion");
	dets.info_switch = '';

	dets.errores_info = document.querySelector(".js--detalle__errores_info");
	dets.estats_info = document.querySelector(".js--detalle__estats_info");

	if (dets.errores_btn) {
		dets.errores_btn.addEventListener("click", function () {
			

			if ( dets.info_switch === '' ) {
				
				this.classList.add("activo");
				dets.errores_info.classList.add("activo");
				dets.info_switch = 'errores';

			} else if ( dets.info_switch === 'estats' ) {
				
				this.classList.add("activo");
				dets.errores_info.classList.add("activo");
				dets.estats_btn.classList.remove("activo");
				dets.estats_info.classList.remove("activo");
				dets.info_switch = 'errores';

			} else if ( dets.info_switch === 'errores' ) {
				
				this.classList.remove("activo");
				dets.errores_info.classList.remove("activo");
				dets.info_switch = '';

			}
		});
	}

	if (dets.estats_btn) {
		dets.estats_btn.addEventListener("click", function () {


			if ( dets.info_switch === '' ) {
				
				this.classList.add("activo");
				dets.estats_info.classList.add("activo");
				dets.info_switch = 'estats';

			} else if ( dets.info_switch === 'errores' ) {
				
				this.classList.add("activo");
				dets.estats_info.classList.add("activo");
				dets.errores_btn.classList.remove("activo");
				dets.errores_info.classList.remove("activo");
				dets.info_switch = 'estats';

			} else if ( dets.info_switch === 'estats' ) {
				
				this.classList.remove("activo");
				dets.estats_info.classList.remove("activo");
				dets.info_switch = '';

			}
		});
	}
}

function drag_detalles() {
	var drag = {};
	drag.contenedor = document.querySelector(".js--detalle--tabla--contenedor");
	drag.draggable = document.querySelector(".js--detalle--drag");

	if (drag.contenedor) {
		drag.draggable.style.top = ( drag.contenedor.clientHeight + 22 ) + 'px';
	}

	$( ".js--detalle--drag" ).draggable({
		axis: "y",
		drag: function( event, ui ) {
			drag.contenedor.style.maxHeight = ui.position.top + 'px';
			drag.contenedor.style.height = ui.position.top + 'px';
		}
	});
}

function input_file() {

	var inputs = document.querySelectorAll( '.js--inputfile' );
	Array.prototype.forEach.call( inputs, function( input ) {
		var label = input.nextElementSibling,
		labelVal = label.innerHTML;

		input.addEventListener( 'change', function( e ) {
			var fileName = '';
			if ( this.files && this.files.length > 1 ) {
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			} else {
				fileName = e.target.value.split( '\\' ).pop();
			}

			if ( fileName ) {
				label.querySelector( 'span' ).innerHTML = fileName;
			} else {
				label.innerHTML = labelVal;
			}
		});

	});
}

function input_text() {

	var inputs_txt = document.querySelectorAll( '.js-input__text--field' );

	Array.prototype.forEach.call( inputs_txt, function( input ) {

		input.addEventListener( 'change', function( ) {

			var valor = input.value;

			if ( valor !== '' ) {
				input.parentNode.classList.add("input--filled");
			} else {
				input.parentNode.classList.remove("input--filled");
			}
		});

	});
}

var win = {};

function lighwindow() {

	win.ver = document.querySelectorAll(".js--lighwindow--ver");
	win.cerrar = document.querySelectorAll(".js--lightwindow--cerrar");
	//win.ventana = document.querySelector(".js--lightwindow");

	Array.prototype.forEach.call( win.ver, function( btn ) {
		btn.addEventListener('click', function() {
			win.lightwindow = btn.getAttribute("data-lightwindow");
			document.querySelector(".js--lightwindow[data-lightwindow='" + win.lightwindow + "']").classList.add("visible");
		});
	});

	Array.prototype.forEach.call( win.cerrar, function( btn ) {
		btn.addEventListener('click', function() {
			document.querySelector(".js--lightwindow[data-lightwindow='" + win.lightwindow + "']").classList.remove("visible");
		});
	});
}

function trimestres() {
	var trims = {};

	trims.btns = document.querySelectorAll(".js--lightwindow--trimestre");
	trims.txt = document.querySelector(".js--trimestre");

	Array.prototype.forEach.call( trims.btns, function( btn ) {
		btn.addEventListener('click', function() {
			trims.trimestre = btn.getAttribute("data-trimestre");
			trims.txt.textContent = trims.trimestre;
			
			document.querySelector(".js--lightwindow[data-lightwindow='" + win.lightwindow + "']").classList.remove("visible");
		});
	});
}

function usuarios() {
	var users = {};

	users.agregar_btn = document.querySelector(".js--agregar__usuario_btn");
	users.agregar = document.querySelector(".js--agregar__usuario");
	users.visible = false;

	users.editar_btn = document.querySelector(".js--editar__usuario_btn");

	if (users.agregar_btn) {
		users.agregar_btn.addEventListener('click', function () {
			
			users.agregar.classList.toggle("visible");
			users.visible = !users.visible;

			if ( users.visible === true) {
				this.innerHTML = "<svg class='usuario--nuevo--icon'><use xlink:href='#close-icon'></use></svg> Cerrar";
				this.classList.add("visible");
			} else {
				this.innerHTML = "<svg class='usuario--nuevo--icon'><use xlink:href='#addBox-icon'></use></svg> Agregar usuario";
				this.classList.remove("visible");
			}
		});
	}

	if (users.editar_btn) {
		users.editar_btn.addEventListener('click', function () {

		});
	}
}

document.addEventListener('DOMContentLoaded', function(){
	detalles();
	drag_detalles();
	input_file();
	input_text();
	lighwindow();
	trimestres();
	usuarios();
});